# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model 42ms
create_cxx_tasks completed in 43ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    [gap of 14ms]
    create-ARM64_V8A-model 11ms
    create-X86_64-model 14ms
    [gap of 16ms]
  create-initial-cxx-model completed in 60ms
create_cxx_tasks completed in 62ms

