package com.example.qrcodeposeestimation;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.qrcodeposeestimation.core.PnPSolution;
import com.example.qrcodeposeestimation.core.PoseEstimation;
import com.example.qrcodeposeestimation.core.Pose;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.FpsMeter;
import org.opencv.android.JavaCamera2View;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.objdetect.QRCodeDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class QrCodeNavigationFragment extends Fragment {

    private static final String TAG = QrCodeNavigationFragment.class.getSimpleName();

    private JavaCamera2View cameraView;
    private TextView distanceTextView;
    private TextView angleTextView;
    private TextView fpsTextView;
    private PointerView pointerView;
    private QrCodeOverlay qrCodeOverlay;

    private long lastScan;

    private FpsMeter fpsMeter;
    private QRCodeDetector qrCodeDetector;
    private ExecutorService executorService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lastScan = System.currentTimeMillis();
        fpsMeter = new FpsMeter();
        executorService = Executors.newSingleThreadExecutor();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_navigation, container, false);

        pointerView = v.findViewById(R.id.pointer);
        distanceTextView = v.findViewById(R.id.distance_text_view);
        angleTextView = v.findViewById(R.id.angle_text_view);
        fpsTextView = v.findViewById(R.id.fps_text_view);
        qrCodeOverlay = v.findViewById(R.id.qrcode_overlay);

        cameraView = v.findViewById(R.id.camera_view);
        cameraView.setVisibility(SurfaceView.VISIBLE);
        cameraView.setCameraPermissionGranted();
        cameraView.setCvCameraViewListener(cvCameraViewListener);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, getContext(), mLoaderCallback);
        } else {
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (cameraView != null)
            cameraView.disableView();
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(getContext()) {
        @Override
        public void onManagerConnected(int status) {
            if (status == LoaderCallbackInterface.SUCCESS) {
                qrCodeDetector = new QRCodeDetector();
                cameraView.enableView();
            } else {
                super.onManagerConnected(status);
            }
        }
    };

    private CameraBridgeViewBase.CvCameraViewListener2 cvCameraViewListener = new CameraBridgeViewBase.CvCameraViewListener2() {
        @Override
        public void onCameraViewStarted(int width, int height) {

        }

        @Override
        public void onCameraViewStopped() {

        }

        @SuppressLint("DefaultLocale")
        @Override
        public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

            getActivity().runOnUiThread(() -> {
                fpsTextView.setText(fpsMeter.measure());
            });

            long t = System.currentTimeMillis();
            if (t - lastScan < 300) {
                return inputFrame.gray();
            }
            lastScan = t;

            Mat mat = inputFrame.gray();

            executorService.execute(() -> {

                Mat points = new Mat();
                List<String> data = new ArrayList<>();
                boolean detected = qrCodeDetector.detectAndDecodeMulti(mat, data, points);

                Point[] p = new Point[4];
                for (int i = 0; i < points.cols(); i++) {
                    p[i] = new Point(points.get(0, i));
                }
                qrCodeOverlay.update(points, data);
                try {
                    if (!points.empty()) {
                            PnPSolution pnpSolution = PoseEstimation.getRelativePose(p);
                            if (pnpSolution.isSolved()) {
                                Pose pose = pnpSolution.getPose();
                                pose.rotation();
                                getActivity().runOnUiThread(() -> {
                                    distanceTextView.setText(String.format("%.3f", pose.distance()));
                                    Mat rvec = pose.getRvec();
//                                    angleTextView.setText(String.format("%.2f %.2f %.2f",
//                                            rvec.get(0, 0)[0], rvec.get(1, 0)[0], rvec.get(2, 0)[0]));
                                    double[] r = pose.rotation();
                                    angleTextView.setText(String.format("%.2f %.2f %.2f", r[0], r[1], r[2]));
                                    pointerView.setRotation((float) r[1]);
                                });
                            }
                        }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });

            return mat;
        }
    };

    public static Fragment getInstance() {
        Bundle args = new Bundle();
        QrCodeNavigationFragment qrCodeNavigationFragment = new QrCodeNavigationFragment();
        qrCodeNavigationFragment.setArguments(args);
        return qrCodeNavigationFragment;
    }
}
