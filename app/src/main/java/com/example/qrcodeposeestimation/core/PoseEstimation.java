package com.example.qrcodeposeestimation.core;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point;

public class PoseEstimation {

    private static final String TAG = PoseEstimation.class.getSimpleName();

    private static final int MIN_POINTS_PNP_NUM = 4;

    private static final float CODE_LENGTH = 165.0f;

    private static final MatOfPoint3f OBJECT_POINTS = new MatOfPoint3f();

    private static final Mat CAMERA_MATRIX = matFrom2DArray(new double[][]{
            new double[] {1375.4480, 0.0, 965.7479},
            new double[] {0.0, 1373.6362, 536.0850},
            new double[] {0.0, 0.0, 1.0},
    });

    private static final MatOfDouble DISTORTION = new MatOfDouble(
            matFrom1DArray(new double[] {0.3115, -1.3962,
                    0.0, 0.0, 2.7731})
    );

    static {
        OBJECT_POINTS.alloc(MIN_POINTS_PNP_NUM);
        OBJECT_POINTS.put(0, 0, -CODE_LENGTH / 2.0, CODE_LENGTH / 2.0, 0.0);
        OBJECT_POINTS.put(1, 0, CODE_LENGTH / 2.0, CODE_LENGTH / 2.0, 0.0);
        OBJECT_POINTS.put(2, 0, CODE_LENGTH / 2.0, -CODE_LENGTH / 2.0, 0.0);
        OBJECT_POINTS.put(3, 0, -CODE_LENGTH / 2.0, -CODE_LENGTH / 2.0, 0.0);
    }


    public static PnPSolution getRelativePose(Point[] points) {
        MatOfPoint2f points2f = new MatOfPoint2f(points);
        Mat rvec = new Mat();
        Mat tvec = new Mat();
        boolean solved = Calib3d.solvePnP(OBJECT_POINTS, points2f, CAMERA_MATRIX, DISTORTION, rvec, tvec);

        return new PnPSolution(solved, new Pose(tvec, rvec));
    }

    private static Mat matFrom1DArray(double[] array) {
        Mat mat = new Mat(1, array.length, CvType.CV_64F);
        mat.put(0, 0, array);
        return mat;
    }

    private static Mat matFrom2DArray(double[][] array) {
        Mat mat = new Mat(array.length, array[0].length, CvType.CV_64F);

        for (int r = 0; r < array.length; r++) {
            mat.put(r, 0, array[r]);
        }
        return mat;
    }
}
