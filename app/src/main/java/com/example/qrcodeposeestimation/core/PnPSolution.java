package com.example.qrcodeposeestimation.core;

public class PnPSolution {

    private boolean solved;
    private Pose pose;

    public PnPSolution(boolean solved, Pose pose) {
        this.solved = solved;
        this.pose = pose;
    }

    public boolean isSolved() {
        return solved;
    }

    public Pose getPose() {
        return pose;
    }
}
