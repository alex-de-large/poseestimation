package com.example.qrcodeposeestimation.core;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

import java.util.Timer;
import java.util.TimerTask;

public class Sensors implements LifecycleObserver {

    private MadgwickAHRS madgwickAHRS;
    private SensorManager sensorManager;
    private Timer updateTimer;

    private float ax, ay, az, gx, gy, gz, mx, my, mz;
    private float[] angles = new float[3];

    private static final String TAG = "Sensors";

    public Sensors(Context context, LifecycleOwner lifecycleOwner) {
        lifecycleOwner.getLifecycle().addObserver(this);
        madgwickAHRS = new MadgwickAHRS(0.01f, 0.00001f);
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void start() {
        Log.d(TAG, "start() called");
        sensorManager.registerListener(aListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST );
        sensorManager.registerListener(mListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_GAME );
        sensorManager.registerListener(gListener, sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_FASTEST);
        updateTimer = new Timer();
        updateTimer.schedule(new UpdateTask(), 1000, 10); // 100Hz
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void stop() {
        sensorManager.unregisterListener(aListener);
        sensorManager.unregisterListener(gListener);
        sensorManager.unregisterListener(mListener);

        updateTimer.cancel();
        updateTimer.purge();
    }

    private void update() {
        madgwickAHRS.update(gx, gy, gz, ax, ay, az, mx, my, mz);
        angles = madgwickAHRS.getEulerAngles();
        Log.d(TAG, getX() + " " + getY() + " " + getZ());
    }

    public float getX() {
        return angles[0];
    }

    public float getY() {
        return angles[1];
    }

    public float getZ() {
        return angles[2];
    }

    private class UpdateTask extends TimerTask {

        @Override
        public void run() {
            update();
        }
    }

    private SensorEventListener gListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            gx = event.values[0];
            gy = event.values[1];
            gz = event.values[2];
            Log.d("Gyroscope", gx + " " + gy + " " + gz);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    private SensorEventListener aListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            ax = event.values[0];
            ay = event.values[1];
            az = event.values[2];
            Log.d("Accelerometer", ax + " " + ay + " " + az);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    private SensorEventListener mListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            mx = event.values[0]; // XY plane
            my = event.values[1]; // XZ plane
            mz = event.values[2]; // ZY plane
            Log.d("Magnetometer", mx + " " + my + " " + mz);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };
}

