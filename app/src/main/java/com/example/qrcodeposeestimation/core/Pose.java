package com.example.qrcodeposeestimation.core;

import android.util.Log;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.Scalar;

public class Pose {

    private Mat tvec;
    private Mat rvec;

    private static final String TAG = Pose.class.getSimpleName();

    public Pose(Mat tvec, Mat rvec) {
        this.tvec = tvec;
        this.rvec = rvec;
    }

    public Mat getTvec() {
        return tvec;
    }

    public Mat getRvec() {
        return rvec;
    }

    public double distance() {
        double xPosFeet = tvec.get(0, 0)[0];
//        double yPosFeet = tvec.get(1, 0)[0];
        double zPosFeet = tvec.get(2, 0)[0];
        return Math.sqrt(xPosFeet * xPosFeet + zPosFeet * zPosFeet);
    }

    public double[] rotation() {

        Mat tvec_map_cam = new MatOfDouble(1.0, 1.0, 1.0);
        Mat R = new Mat(3, 3, CvType.CV_32FC1);
        Calib3d.Rodrigues(rvec, R);
        // transpose R, because we need the transformation from
        // world(map) to camera
        R = R.t();
        // get rotation around X,Y,Z from R
        double bankX = Math.toDegrees(Math.atan2(-R.get(1, 2)[0], R.get(1, 1)[0]));
        double headingY = Math.toDegrees(Math.atan2(-R.get(2, 0)[0], R.get(0, 0)[0]));
        double attitudeZ = Math.toDegrees(Math.asin(R.get(1, 0)[0]));
        // compute translation vector from world (map) to cam
        // tvec_map_cam
        Core.multiply(R, new Scalar(-1), R); // R=-R
        Core.gemm(R, tvec, 1, new Mat(), 0, tvec_map_cam, 0); // tvec_map_cam=R*tvec
        R.release();

//        org.ros.rosjava_geometry.Quaternion rotation = new org.ros.rosjava_geometry.Quaternion(q.getX(), q.getY(),
//                q.getZ(), q.getW());
        double x = tvec_map_cam.get(0, 0)[0];
        double y = tvec_map_cam.get(1, 0)[0];
        double z = tvec_map_cam.get(2, 0)[0];
        tvec_map_cam.release();

//        return new double[] {x, y, z};
        return new double[] {bankX, headingY, attitudeZ};
    }

//    public double angleXZ() {
//        return Math.toDegrees(Math.atan(tvec.get(0, 0)[0] / tvec.get(2, 0)[0]));
//    }
}
