package com.example.qrcodeposeestimation.core;

import java.util.Hashtable;

public class Map {

    private Hashtable<String, MapObject> objectTable;

    public Map() {
        objectTable = new Hashtable<>();
    }

    public void add(String id, MapObject mapObject) {
        objectTable.put(id, mapObject);
    }

    public MapObject get(String id) {
        return objectTable.get(id);
    }
}
