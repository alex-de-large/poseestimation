package com.example.qrcodeposeestimation.core;

public class MapObject {

    public double x;
    public double y;

    public MapObject(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double distance(MapObject o) {
        return distance(o.x, o.y);
    }

    public double distance(double x, double y) {
        return Math.sqrt(x * x + y * y);
    }
}
