package com.example.qrcodeposeestimation.core;

public class Device extends MapObject {

    public double rx;
    public double ry;
    public double rz;

    public Device(double x, double y, double rx, double ry, double rz) {
        super(x, y);
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
    }

    public double angle(MapObject mapObject) {
        double a = this.x - mapObject.x;
        double b = this.y - mapObject.y;
        double degrees = (float) Math.toDegrees(Math.atan(a / b));
        if (this.x > mapObject.x) {
            degrees = 180.f + degrees;
        }
        if (Double.isNaN(degrees)) {
            return 0.0f;
        } else if (degrees == 0 && this.x > mapObject.x) {
            return 180.0f;
        }
        return degrees;
    }
}
