package com.example.qrcodeposeestimation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import org.opencv.core.Mat;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

public class QrCodeOverlay extends AppCompatImageView {

    private Mat points;
    private List<String> data;
    private Paint paint;

    private static final int STROKE_WIDTH = 10;
    private static final int TEXT_STROKE_WIDTH = 5;

    private static final String TAG = QrCodeOverlay.class.getSimpleName();

    public QrCodeOverlay(@NonNull Context context) {
        super(context);
    }

    public QrCodeOverlay(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(STROKE_WIDTH);

        data = new ArrayList<>();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        if (points == null) {
            return;
        }

        Log.d(TAG, getWidth() + "x" + getHeight());

        canvas.scale((float) getWidth() / 1280.0f, (float) getHeight() / 720.0f);

        for (int i = 0; i < points.cols(); i++) {
            Point pt1 = new Point(points.get(0, i));
            Point pt2 = new Point(points.get(0, (i + 1) % 4));
//            Imgproc.line(mat, pt1, pt2, new Scalar(255, 0, 0), 3);
            canvas.drawLine((float) pt1.x, (float) pt1.y, (float) pt2.x, (float) pt2.y, paint);

        }
    }

    public void update(Mat points, List<String> data) {
        this.points = points;
        this.data = data;
        invalidate();
    }

}
