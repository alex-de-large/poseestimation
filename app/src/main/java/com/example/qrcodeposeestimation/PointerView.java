package com.example.qrcodeposeestimation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

public class PointerView extends AppCompatImageView {

    private float currentRotation;

    public PointerView(Context context) {
        super(context);
    }

    public PointerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void rotate(float angle) {
        float newRotation = currentRotation + angle;
        update(currentRotation, newRotation);
        currentRotation = newRotation;
    }

    public void setRotation(float angle) {
        update(currentRotation, angle);
        currentRotation = angle;
    }

    private void update(float oldRotation, float newRotation) {
        RotateAnimation ra = new RotateAnimation(
                oldRotation,
                newRotation,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);
        ra.setDuration(250);
        ra.setFillAfter(true);
        this.startAnimation(ra);
    }

}
